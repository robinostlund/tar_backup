#!/usr/bin/env python
# Created by me@robinostlund.name
import os
import sys
import time
import shutil
import tarfile

from optparse import OptionParser

class TarBackup(object):
    def __init__(self, options):
        self.backup_source_folder = options.source_path
        self.backup_target_folder = options.destination_path
        self.backup_excludes = options.excludes.split(',')
        self.backup_minimum_free_diskspace = int(options.minimum_free_space)
        self.backup_day_of_week_to_keep = int(options.day_of_week_to_keep)
        self.backup_days_to_keep = int(options.days_to_keep)
        self.backup_weeks_to_keep = int(options.weeks_to_keep)
        self.backup_overwrite_old_backups = int(options.overwrite_old_backups)
        self.verbose = options.verbose

        # Set some time variables
        self.time_todays_date = time.strftime('%d')
        self.time_todays_month = time.strftime('%m')
        self.time_full = time.strftime('%Y-%m-%d.%H:%M:%S')
        self.time_full_time = time.strftime('%H:%M:%S')
        self.time_full_date = time.strftime('%Y-%m-%d')
        self.time_week_number = time.strftime('%W')
        self.time_weekday = time.strftime('%w')
        self.time_weekday_decimal = time.strftime('%w')
        self.time_weekday_name = time.strftime('%a')
        self.time_weekday_full_name = time.strftime('%A')

    def _StartBackup(self):
        if not self._EnsureFolderExist(self.backup_target_folder):
            raise BaseException('ERROR: Target folder does not exist.')

        if not self._EnsureFolderExist(self.backup_source_folder):
            if not self._EnsureFileExist(self.backup_source_folder):
                raise BaseException('ERROR: Source folder does not exist.')

        # Start daily backups
        file_name_daily = os.path.join(self.backup_target_folder, '%s-%s-daily.tar.gz' % (os.path.basename(self.backup_source_folder), self.time_full_date))
        if self._EnsureFileExist(file_name_daily) and not self.backup_overwrite_old_backups:
            raise BaseException('ERROR: Target file %s already exist.' % (file_name_daily))

        if self._EnsureFileExist(file_name_daily) and self.backup_overwrite_old_backups:
            print('INFO: Target file %s already exist, overwriting' % (file_name_daily))

        if self.backup_minimum_free_diskspace > self._GetFreeSize(self.backup_target_folder):
            raise BaseException('ERROR: not enough free space on %s.' % (self.backup_target_folder))

        print('INFO: Creating daily backup of %s to %s.' % (self.backup_source_folder, file_name_daily))
        self._CreateTarFile(file_name_daily)
        (size, value) = self._ConvertSizeToHumanReadable(self._GetFileSize(file_name_daily))
        print('INFO: Backup completed, filesize of %s (%s%s)' % (file_name_daily, size, value))

        # Start weekly backups
        if int(self.time_weekday) == self.backup_day_of_week_to_keep:
            file_name_weekly = os.path.join(self.backup_target_folder, '%s-%s-weekly.tar.gz' % (os.path.basename(self.backup_source_folder), self.time_full_date))
            if self._EnsureFileExist(file_name_weekly) and not self.backup_overwrite_old_backups:
                raise BaseException('ERROR: target file %s already exist.' % (file_name_weekly))

            print('INFO: Copying daily backup %s to %s.' % (file_name_daily, file_name_weekly))
            self._CopyFile(file_name_daily, file_name_weekly)
            (size, value) = self._ConvertSizeToHumanReadable(self._GetFileSize(file_name_weekly))
            print('INFO: Copy completed, filesize of %s (%s%s)' % (file_name_weekly, size, value))


    def _CopyFile(self, source_file, target_file):
        shutil.copy2(source_file, target_file)

    def _RemoveOldBackups(self):
        weekly_files = []
        for file_name in os.listdir(self.backup_target_folder):
            full_file_name = os.path.join(self.backup_target_folder, file_name)
            file_stat = self._GetFileStat(full_file_name)
            if 'daily' in file_name:
                daily_seconds_diff = time.time() - self.backup_days_to_keep * 86400
                if daily_seconds_diff > file_stat.st_mtime:
                    print('INFO: Removing %s, it is older than %s days' % (full_file_name, self.backup_days_to_keep))
                    self._RemoveFile(full_file_name)

            elif 'weekly' in file_name:
                weekly_files.append({'full_file_name': full_file_name, 'last_modified': file_stat.st_mtime})

        # Remove weeklybackups if more than self.backup_day_of_week_to_keep
        if len(weekly_files) > self.backup_weeks_to_keep:
            remove_number_of_backups = len(weekly_files) - self.backup_weeks_to_keep
            for file in sorted(weekly_files, key= lambda k: k['last_modified']):
                if remove_number_of_backups > 0:
                    self._RemoveFile(file['full_file_name'])
                    remove_number_of_backups -= 1
                    print('INFO: Removing file %s.' % (file['full_file_name']))

    def _CreateTarFile(self, file_name):
        tar = tarfile.open(file_name, 'w:gz')
        if self._EnsureFileExist(self.backup_source_folder):
            if self.verbose: print('DEBUG: adding file %s to tar archive' % (self.backup_source_folder))
            tar.add(self.backup_source_folder)
        else:
            self._AddToTarFile(tar, self.backup_source_folder)
        tar.close()

    def _AddToTarFile(self, tar, path):
        basedir = path
        subdirlist = []
        for item in os.listdir(path):
            if not self._ExcludeFileInTar(os.path.join(basedir,item)):
                if os.path.isdir(os.path.join(basedir, item)):
                    subdirlist.append(os.path.join(basedir, item))
                tar.add(os.path.join(basedir,item), recursive=False)
                tar.members = []

        for subdir in subdirlist:
            if not self._ExcludeFileInTar(subdir):
                self._AddToTarFile(tar, subdir)

    def _ExcludeFileInTar(self, path):
        if path in self.backup_excludes:
            return True

        return False

    def _RemoveFile(self, file_path):
        try:
            os.remove(file_path)
            return True
        except OSError as e:
            return False

    def _RemoveFolder(self, folder_path):
        try:
            os.rmdir(folder_path)
            return True
        except OSError as e:
            return False

    def _CreateFolder(self, folder_path):
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

    def _GetFileSize(self, file_path):
        size = os.path.getsize(file_path)
        return size

    def _GetFileStat(self, file_path):
        stat = os.stat(file_path)
        return stat


    def _GetFreeSize(self, folder_path):
        stats = os.statvfs(folder_path)
        free_size_bytes = stats.f_bavail * stats.f_frsize
        free_size_kbytes = free_size_bytes / 1024
        free_size_mbytes = free_size_kbytes / 1024
        free_size_gbytes = free_size_mbytes / 1024
        free_size_tbytes = free_size_gbytes / 1024

        return int(free_size_mbytes)

    def _EnsureFolderExist(self, folder_path):
        return os.path.isdir(folder_path)

    def _EnsureFileExist(self, file_path):
        return os.path.isfile(file_path)

    def _ConvertBytesToKb(self, file_size):
        kb = file_size / 1024
        return kb

    def _ConvertBytesToMb(self, file_size):
        mb = file_size / 1024 / 1024
        return mb

    def _ConvertBytesToGb(self, size):
        gb = size / 1024 / 1024 / 1024
        return gb

    def _ConvertSizeToHumanReadable(self, bytes):
        size = bytes
        unit = 'b'
        if bytes > 1024:
            size = self._ConvertBytesToKb(bytes)
            unit = 'kb'
            if bytes > 1048576:
                size = self._ConvertBytesToMb(bytes)
                unit = 'mb'
                if bytes > 1073741824:
                    size = self._ConvertBytesToGb(bytes)
                    unit = 'gb'

        return (size, unit)

def CheckIfPidIsRunning(pid):
    try:
        os.kill(int(pid), 0)
    except OSError:
        return False
    else:
        return True

def CreatePidFile(pidfile):
    if os.path.exists(pidfile):
        old_pid = open(pidfile, 'r').read()
        if CheckIfPidIsRunning(old_pid):
            return True
        else:
            os.unlink(pidfile)
    open(pidfile, 'w').write(str(os.getpid()))
    return False


def RemovePidFile(pidfile):
    os.unlink(pidfile)



def option_parser():
    parser = OptionParser(add_help_option = True)
    parser.add_option('-s', '--source-path', dest = 'source_path', action = 'store', default = False, help = 'specify source path (required)')
    parser.add_option('-d', '--destination-path', dest = 'destination_path', action = 'store', default = False, help = 'specify destination path (required)')
    parser.add_option('-e', '--excludes', dest='excludes', action='store', default = '', help='specify excludes (comma separated)')
    parser.add_option('-m', '--free-space', dest='minimum_free_space', action='store', default=0, help='specify minimum free space in mb (default 0)')
    parser.add_option('-o', '--overwrite-old-backups', dest='overwrite_old_backups', action='store_true', default=True, help='specify minimum free space in mb (default: True')
    parser.add_option('-p', '--pid-file', dest='pid_file', action='store', default= '/tmp/.tar_backup.pid', help = 'specify pid file (default: /tmp/.tar_backup.pid')
    parser.add_option('-D', '--days-to-keep', dest='days_to_keep', action='store', default = 7, help='specify how many days to keep daily backup (default 7)')
    parser.add_option('-O', '--day-of-week', dest='day_of_week_to_keep', action='store', default=1, help='specify which day to keep weekly backups from (default 1 (Monday)')
    parser.add_option('-W', '--weeks-to-keep', dest='weeks_to_keep', action='store', default=1,  help='specify how many weeks to keep weekly backups (default 1 week)')
    parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='show verbose information (default False)')

    (options, args) = parser.parse_args()

    if not os.path.isdir(options.source_path):
        if not os.path.isfile(options.source_path):
            print('ERROR: Source path %s does not exists.' % (options.source_path))
            parser.print_help()
            sys.exit(2)

    if not os.path.isdir(options.destination_path):
        print('ERROR: Destination path %s does not exists.' % (options.destination_path))
        parser.print_help()
        sys.exit(2)

    return options

def main():
    options = option_parser()
    if CreatePidFile(options.pid_file):
        raise BaseException('ERROR: folder_backup is already running.')

    backup_process = TarBackup(options)
    backup_process._RemoveOldBackups()
    backup_process._StartBackup()

    RemovePidFile(options.pid_file)

if __name__ == '__main__':
    main()

